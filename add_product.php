<!DOCTYPE html>
<html>
<head>
	<title>Add product</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style.css" type="text/css">

</head>
<body>
	<div class="header">
	  	<h1> Add product</h1>
	  	<img src="Images/scandiweb.jpg" alt="Scandiweb logo">
	</div>
	<div class="footer">
		<button onclick="location.href = 'index.php';">Back</button>
	</div>

<?php
	include "db_connection.php";
	include "bootstrap_add_product_form.php";
	$link = mysqli_connect("localhost", "root", "usbw", "test");
	#$new_product_SKU = $_GET["new_product_SKU"];
	#$new_product_name = $_GET["new_product_name"];
	#$new_product_price = $_GET["new_product_price"];
	#$new_product_type = $_GET["new_product_type"];


#defining variables to insert specific fields in database and show in browser
#words defined in '' is name from bootstrap_add_product_form.php name field 
	if (isset($_POST['act'])){
		if ($_POST['act']=="add_product"){

			$honeypot = $_POST['email'];
			if ($honeypot){ exit; }

			$new_product_SKU = mysqli_real_escape_string($link, $_POST['new_product_SKU']);
			$new_product_name = mysqli_real_escape_string($link, $_POST['new_product_name']);
			$new_product_price = mysqli_real_escape_string($link, $_POST['new_product_price']);
			$new_product_type = mysqli_real_escape_string($link, $_POST['new_product_type']);

			$sql = "INSERT INTO products (SKU, Name, Price, Type) VALUES ('$new_product_SKU','$new_product_name','$new_product_price','$new_product_type')";
			
			if(mysqli_query($link, $sql)){

		    	echo "Products inserted successfully.";
			} 
		}
	}
	
?>
</body>
</html>

