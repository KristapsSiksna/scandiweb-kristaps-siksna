<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<?php

  $sql = "SELECT * FROM products";
  $result = $mysqli->query($sql);

  if ($result->num_rows > 0) {
    ?>
    <div class="grid-container">
    <?php
      // output data of each row
      while($row = $result->fetch_assoc()) {
        ?>
          <div class="grid-item">
          <?php echo "SKU: ". $row['SKU']; ?> <br>
            <?php echo "Name: ". $row['Name']; ?> <br>
            <?php echo "Type: ". $row['Type']; ?> <br>
            <?php echo "$: ". $row['Price']; ?><br>
          </div>
      <?php } ?>
    </div> 
<?php
} else {
  echo "0 results";
}
?>


</body>
</html>