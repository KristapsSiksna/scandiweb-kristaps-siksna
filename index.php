<!DOCTYPE html>
<html>
<head>
	<title>Product list</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style.css" type="text/css">

</head>
<body>
	<div class="header">

		<img src="Images/scandiweb.jpg" alt="Scandiweb logo">
	  	<h1> Product list</h1><br>
		<button onclick="location.href = 'add_product.php';">Add new product</button>
		
	</div>
<?php
#connecting to database by including connection file
	include "db_connection.php";
#dislplaying all products by including file
	include "all_products.php";
	
?>

<?php
#close connection with database
	$mysqli->close();
?>
</body>
</html>