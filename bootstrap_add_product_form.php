<script src="form_validtion.js"></script>
  <!-- CSS for add product page-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="CSS/style.css" type="text/css">



<form class="form-horizontal" action="add_product.php" method="post">
  <fieldset>
    <!-- SKU input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="SKU">SKU</label>  
      <div class="col-md-4">
      <input id="SKU" name="new_product_SKU" type="varchar" placeholder="SKU" class="form-control input-md" >
        
      </div>
    </div>

    <!-- Name input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="Name">Name</label>  
      <div class="col-md-4">
      <input id="Name" name="new_product_name" type="text" placeholder="Name" class="form-control input-md" >
        
      </div>
    </div>

    <!-- Price input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="Price">Price</label>  
      <div class="col-md-4">
      <input id="Price" name="new_product_price" type="number" placeholder="0.0" class="form-control input-md" >
        
      </div>
    </div>

    <!-- Type input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="Type">Type</label>  
      <div class="col-md-4">

      <input type="radio" id="Type" name="new_product_type" value="DVD">
      <label for="Type">DVD</label><br>
      <input type="radio" id="Type" name="new_product_type" value="Book">
      <label for="Type">Book</label><br>
      <input type="radio" id="Type" name="new_product_type" value="Furniture">
      <label for="Type">Furniture</label>
  
        
      </div>
    </div>

    <!-- Add Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="Submitbutton"></label>
      <div class="col-md-4">
        <button onclick="location.href = 'index.php';" id="Submitbutton" name="Submitbutton" class="btn btn-primary" href="javascript:window.location.reload();">Add</button>
        
      </div>
    </div>
    <input type="hidden" name="act" value="add_product">
    <input type="text" name="email" style="display: none;">
  </fieldset>
</form>